package com.glearning.blog.dto;

import java.time.LocalDate;
import java.util.Set;

import lombok.Data;

@Data
public class PostDto {
	
	private long id;
	private String title;
	
	private String description;
	
	private String content;
	
	private LocalDate postedDate;

	
	private Set<CommentDto> comments;



}
