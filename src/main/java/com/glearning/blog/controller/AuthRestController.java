package com.glearning.blog.controller;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.blog.dto.JwtAuthResponse;
import com.glearning.blog.dto.LoginDto;
import com.glearning.blog.dto.SignupDto;
import com.glearning.blog.entity.Role;
import com.glearning.blog.entity.User;
import com.glearning.blog.repository.RoleRepository;
import com.glearning.blog.repository.UserRepository;
import com.glearning.blog.security.JwtTokenProvider;


@RestController
@RequestMapping("/api/v1/auth")
public class AuthRestController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<JwtAuthResponse> authenticateUser(@RequestBody LoginDto loginDto) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginDto.getUsernameOrEmail(), loginDto.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		// get token from JwtTokenProvider
		String token = jwtTokenProvider.generateToken(authentication);

		return ResponseEntity.ok(new JwtAuthResponse(token));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody SignupDto signupDto) {

		// add a check if username already exists
		if (userRepository.existsByUsername(signupDto.getUsername())) {
			return new ResponseEntity<>("username is already taken", HttpStatus.BAD_REQUEST);
		}

		// add a check if email already exists
		if (userRepository.existsByEmail(signupDto.getEmail())) {
			return new ResponseEntity<>("email is already taken", HttpStatus.BAD_REQUEST);
		}

		User user = new User();
		user.setEmail(signupDto.getEmail());
		user.setName(signupDto.getName());
		user.setUsername(signupDto.getUsername());
		user.setPassword(passwordEncoder.encode(signupDto.getPassword()));

		Role userRole = new Role();
		userRole.setName("ROLE_USER");
		this.roleRepository.save(userRole);
		Role roles = roleRepository.findByName("ROLE_USER").get();
		user.setRoles(Collections.singleton(roles));

		userRepository.save(user);

		return new ResponseEntity<>("User successfully registered", HttpStatus.OK);
	}
}
