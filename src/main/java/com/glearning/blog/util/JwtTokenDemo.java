package com.glearning.blog.util;

import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtTokenDemo {
	
	public static void main(String[] args) {
		String token = Jwts.builder().setSubject("pradeep").setIssuedAt(new Date()).setExpiration(new Date() )
				.signWith(SignatureAlgorithm.HS512, "thisIsNoMoreASecret").compact();
		
		System.out.println(token);
		
		Claims claims = Jwts.parser().setSigningKey("thisIsNoMoreASecret").parseClaimsJws(token).getBody();

		System.out.println(claims.getSubject());
	}

}
