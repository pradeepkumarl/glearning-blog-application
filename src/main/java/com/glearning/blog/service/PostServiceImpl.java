package com.glearning.blog.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.glearning.blog.dto.PostDto;
import com.glearning.blog.dto.PostResponse;
import com.glearning.blog.entity.Post;
import com.glearning.blog.exception.ResourceNotFoundException;
import com.glearning.blog.repository.PostRepository;

@Service
public class PostServiceImpl implements PostService {

	private PostRepository postRepository;

	private ModelMapper modelMapper;

	public PostServiceImpl(PostRepository postRepository, ModelMapper modelMapper) {
		this.postRepository = postRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public PostDto createPost(PostDto postDto) {
		// convert DTO to Entity
		Post post = mapToEntity(postDto);
		Post savedPost = this.postRepository.save(post);
		// convert entity to DTO
		PostDto postResponse = mapToDto(savedPost);
		return postResponse;
	}

	@Override
	public PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir) {
		Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();

		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		Page<Post> posts = this.postRepository.findAll(pageable);
		List<Post> listOfPosts = posts.getContent();
		List<PostDto> listOfPostDto = listOfPosts.stream().map(post -> mapToDto(post)).collect(Collectors.toList());

		PostResponse postResponse = new PostResponse();

		postResponse.setContent(listOfPostDto);
		postResponse.setPageNo(posts.getNumber());
		postResponse.setPageSize(posts.getSize());
		postResponse.setTotalElements(posts.getTotalElements());
		postResponse.setTotalPages(posts.getTotalPages());
		postResponse.setLast(posts.isLast());

		return postResponse;
	}

	@Override
	public PostDto getPostById(long id) {

		Post post = this.postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
		return mapToDto(post);
	}

	@Override
	public PostDto updatePost(PostDto postDto, long id) {
		// fetch the existing post with the given post id
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
		post.setDescription(postDto.getDescription());
		post.setTitle(postDto.getTitle());
		post.setContent(postDto.getContent());

		// save the updated post to the db using the post repository
		Post updatedPost = this.postRepository.save(post);
		PostDto updatedPostDto = this.mapToDto(updatedPost);
		return updatedPostDto;
	}

	@Override
	public void deletePostById(long id) {
		/*
		 * Post post = postRepository.findById(id).orElseThrow(()-> new
		 * ResourceNotFoundException("Post", "id", id));
		 * this.postRepository.delete(post);
		 */
		this.postRepository.deleteById(id);
	}

	private Post mapToEntity(PostDto postDto) {
		Post post = modelMapper.map(postDto, Post.class);
		return post;

	}

	// convert entity to DTO
	private PostDto mapToDto(Post post) {
		PostDto postDto = modelMapper.map(post, PostDto.class);
		return postDto;

	}

}
