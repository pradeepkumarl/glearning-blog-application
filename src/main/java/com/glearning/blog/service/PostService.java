package com.glearning.blog.service;

import com.glearning.blog.dto.PostDto;
import com.glearning.blog.dto.PostResponse;

public interface PostService {
	
	PostDto createPost(PostDto postDto);

	PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir);

	PostDto getPostById(long id);

	PostDto updatePost(PostDto postDto, long id);

	void deletePostById(long id);
}
